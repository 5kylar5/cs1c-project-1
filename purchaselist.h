/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef PURCHASELIST_H
#define PURCHASELIST_H

#include <QWidget>


/*! \namespace Ui
    \brief class PurchaseList.

    This namespace contains the class PurchaseList.
*/
namespace Ui {
class PurchaseList;
}

//!  PurchaseList class.
/*!
  This class contains all the public and private data members/methods associated with the PurchaseList.
*/
class PurchaseList : public QWidget
{
    Q_OBJECT

public:

    //! PurchaseList constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit PurchaseList(QWidget *parent = nullptr);

    //! PurchaseList destructor.
        /*!
         This destructs the PurchaseList UI.
        */
    ~PurchaseList();

private:

    //! A private variable.
      /*!
        Pointer that points to the user interface of the PurchaseList
      */
    Ui::PurchaseList *ui;

};

#endif // PURCHASELIST_H
