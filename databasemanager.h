/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <customer.h>
#include <QString>
#include <QSqlDatabase>
#include <QVector>

//!  DatabaseManager class.
/*!
  This class contains all the public and private data members/methods associated with the DatabaseManager.
*/
class DatabaseManager
{
public:
    //! DatabaseManager constructor.
       /*!
         Performs database initializations.
       */
    DatabaseManager();

    //! DatabaseManager destructor.
        /*!
         This destructs the DatabaseManager.
        */
    ~DatabaseManager();

    //! Adds a customer to the database.
       /*!
         \param customer the customer
         \return Bool
       */
    bool AddCustomer(Customer customer);

    //! Gets customer by he or she's ID
       /*!
         \param Id the ID
         \return Customer
       */
    Customer GetCustomerById(int Id);

    //! Gets customer by he or she's Name
       /*!
         \param name the name
         \return Customer
       */
    Customer GetCustomerByName(QString name);

    //! Deletes customer by he or she's ID
       /*!
         \param Id the ID
         \return nothing
       */
    void DeleteCustomerById(int Id);

    //! Updates customer
       /*!
         \param updatedCustomer the Customer object
         \return nothing
       */
    void UpdateCustomer(Customer updatedCustomer);

    //! Gets customer list
       /*!
         \param list the list of customers objects
         \param size the size of list
         \return nothing
       */
    void GetList(Customer list[], int &size);


private:
    //! A private variable.
      /*!
        The SQL database.
      */
       QSqlDatabase database;

       //! A private variable.
         /*!
           The File path.
         */
       QString FilePath;
};

#endif // DATABASEMANAGER_H
