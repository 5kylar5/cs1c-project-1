
function Component () { } // constructor

Component.prototype.isDefault = function () {
    return true; // select the component by default
}

Component.prototype.createOperations = function () {
    try {
        component.createOperations (); // call the base create operations function
        if (installer.value ("os") === "win") {
            [
                { "name" : "QtCAN TestBench",        "exe" : "QtCAN-tool-CanTestBench", "icon" : "testbench", "menu" : true, "desktop" : true  },
                { "name" : "QtCAN Sniffer",          "exe" : "QtCAN-tool-CanSniffer",   "icon" : "sniffer",   "menu" : true, "desktop" : true  },
                { "name" : "QtCAN Bridge",           "exe" : "QtCAN-tool-CanBridge",    "icon" : "bridge",    "menu" : true, "desktop" : true  },
                { "name" : "QtCAN Maintenance Tool", "exe" : "@MaintenanceToolName@",   "icon" : "",          "menu" : true, "desktop" : false },
            ].forEach (function (item) {
                if (item ["menu"]) {
                    component.addOperation ("CreateShortcut",
                                            "@TargetDir@/" + item ["exe"] + ".exe",
                                            "@StartMenuDir@/" + item ["name"] + ".lnk",
                                            "workingDirectory=@TargetDir@",
                                            "iconPath=@TargetDir@/" + item ["icon"] + ".ico");
                }
                if (item ["desktop"]) {
                    component.addOperation ("CreateShortcut",
                                            "@TargetDir@/" + item ["exe"] + ".exe",
                                            "@DesktopDir@/" + item ["name"] + ".lnk",
                                            "workingDirectory=@TargetDir@",
                                            "iconPath=@TargetDir@/" + item ["icon"] + ".ico");

                }
            });
        }
    }
    catch (e) {
        print (e);
    }
}
