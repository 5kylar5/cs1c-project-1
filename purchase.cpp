#include "purchase.h"

Purchase::Purchase()
{
    EmailSelected = 0;
    NetworkSelected = 0;
    CloudSelected = 0;
    MaintenceSelected = 0;
}

Purchase::Purchase(class Customer customer, int email, int network, int cloud, int maintence)
{
    Customer = customer;
    EmailSelected = email;
    NetworkSelected = network;
    CloudSelected = cloud;
    MaintenceSelected = maintence;
}
Purchase::~Purchase()
{

}

void Purchase::SetId(int id)
{
    Id = id;
}

int Purchase::GetId()
{
    return Id;
}

void Purchase::SetCustomer(class Customer customer)
{
    Customer = customer;
}
void Purchase::SetEmail(int email)
{
    EmailSelected = email;
}
void Purchase::SetNetwork(int network)
{
    NetworkSelected = network;
}
void Purchase::SetCloud(int cloud)
{
    CloudSelected = cloud;
}

void Purchase::BuildPlanString()
{
    QString plan;
    if(EmailSelected)
        plan += SERVICES[0] + ", ";
    if(NetworkSelected)
        plan += SERVICES[1] + ", ";
    if(CloudSelected)
        plan += SERVICES[2] + ", ";
    if(MaintenceSelected)
        plan += SERVICES[3];

    PlanString = plan;
}

void Purchase::GetTotalPrice()
{
    int total = 0;
    if(EmailSelected)
        total += PRICES[0];
    if(NetworkSelected)
        total += PRICES[1];
    if(CloudSelected)
        total += PRICES[2];
    if(MaintenceSelected)
        total += PRICES[3];

    TotalPrice = total;
}


void Purchase::SetMaintence(int maintence)
{
    MaintenceSelected = maintence;
}

Customer Purchase::GetCustomer()
{
    return Customer;
}
int Purchase::GetEmail()
{
    return EmailSelected;
}
int Purchase::GetNetwork()
{
    return NetworkSelected;
}
int Purchase::GetCloud()
{
    return CloudSelected;
}
int Purchase::GetMaintence()
{
    return MaintenceSelected;
}


QString Purchase::GetPlans()
{
    return PlanString;
}
int Purchase::GetPrice()
{
    return TotalPrice;
}
