/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

/*! \namespace Ui
    \brief class MainWindow.

    This namespace contains the class MainWindow.
*/
namespace Ui {
class MainWindow;
}

//!  MainWindow class.
/*!
  This class contains all the public and private data members/methods associated with the MainWindow.
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    //! MainWindow constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit MainWindow(QWidget *parent = nullptr);

    //! MainWindow destructor.
        /*!
         This destructs the main window UI.
        */
    ~MainWindow();

private slots:

    //! Opens the admin portal.
       /*!
         \return Nothing. Function is void.
       */
    void on_adminButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! Opens the pamphlet window.
       /*!
         \return Nothing. Function is void.
       */
    void on_pamphletButton_clicked();

private:

    //! A private variable.
      /*!
        Pointer that points to the user interface of the main window
      */
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
