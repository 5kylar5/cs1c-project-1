/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef TESTIMONIALCALSS_H
#define TESTIMONIALCALSS_H

#include "customer.h"

//!  Testimonial class.
/*!
  This class contains all the public and private data members/methods associated with a testimonial.
*/
class TestimonialCalss
{
public:

    //! Testimonial class default constructor.
          /*!
            This constructor sets the data associated with the testimonial
          */
    TestimonialCalss();

    //! Testimonial class non default constructor.
          /*!
            This constructor sets private data memebers to their associated parameters.
            \param customer the customer
            \param id the id assoicated to custoemr
            \param text the testimonial text
          */
    TestimonialCalss(class Customer customer, int id, QString text);

    //! Testimonial destructor.
        /*!
         This destructs the purchase object.
        */
    ~TestimonialCalss();

    //! Sets the customers.
        /*!
          \param customer the customer
          \return Nothing. Function is void.
        */
    void SetCustomer(class Customer customer);

    //! Sets the ID.
        /*!
          \param id the ID
          \return Nothing. Function is void.
        */
    void SetId(int id);

    //! Sets the Text.
        /*!
          \param text the testimonial text
          \return Nothing. Function is void.
        */
    void SetText(QString text);

    //! Gets the customers.
        /*!
          \return Customer object
        */
    Customer GetCustomer();

    //! Gets the customers ID.
        /*!
          \return Int
        */
    int GetId();

    //! Gets the text.
        /*!
          \return QString
        */
    QString GetText();

private:

    //! A private variable.
      /*!
        Customer object
      */
    Customer Customer;

    //! A private variable.
      /*!
        Text of the testimonial
      */
    QString Text;

    //! A private variable.
      /*!
        Id of the customer
      */
    int Id;
};

#endif // TESTIMONIALCALSS_H
