/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "adminportal.h"
#include "ui_adminportal.h"
#include "mainwindow.h"
#include "adminaddcustomer.h"
#include "admincustomerlist.h"
#include "adminremovecustomer.h"
#include "admineditcustomer.h"
#include "adminviewpurchases.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QtDebug>


AdminPortal::AdminPortal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminPortal)
{
    ui->setupUi(this);
}

AdminPortal::~AdminPortal()
{
    delete ui;
}

void AdminPortal::on_logoutButton_clicked()
{
    MainWindow *main = new MainWindow();
    main -> show();
    this -> close();
}

void AdminPortal::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("Through the admin portal you're able to display the customer list, add/remove a customer, edit a customer and view purchases");
        msgBox.exec();
}

void AdminPortal::on_showButton_clicked()
{
    AdminCustomerList *customerList = new AdminCustomerList();
    customerList -> show();
    this -> close();
}

void AdminPortal::on_addButton_clicked()
{
    AdminAddCustomer *addCustomer = new AdminAddCustomer();
    addCustomer -> show();
    this -> close();
}

void AdminPortal::on_removeButton_clicked()
{
    AdminRemoveCustomer *removeCustomer = new AdminRemoveCustomer();
    removeCustomer -> show();
    this -> close();
}

void AdminPortal::on_editButton_clicked()
{
    AdminEditCustomer *editCustomer = new AdminEditCustomer();
    editCustomer -> show();
    this -> close();
}

void AdminPortal::on_viewPurchasesButton_clicked()
{
    AdminViewPurchases *viewPurchases = new AdminViewPurchases();
    viewPurchases -> show();
    this -> close();
}


