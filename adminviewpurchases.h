/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef ADMINVIEWPURCHASES_H
#define ADMINVIEWPURCHASES_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminViewPurchases.

    This namespace contains the class AdminViewPurchases.
*/
namespace Ui {
class AdminViewPurchases;
}

//!  AdminViewPurchases class.
/*!
  This class contains all the public and private data members/methods associated with the View Purchases Form within the admin portal.
*/
class AdminViewPurchases : public QWidget
{
    Q_OBJECT

public:
    //! AdminViewPurchases constructor.
           /*!
             This constructor sets up the UI.
           */
    explicit AdminViewPurchases(QWidget *parent = nullptr);

    //! AdminViewPurchases destructor.
          /*!
           This destructs the view purchases UI.
          */
    ~AdminViewPurchases();

private slots:

    //! Allows the user to return to the admin portal.
           /*!
             \return Nothing. Function is void.
           */
    void on_backButton_clicked();

    //! Displays the Help for that window.
          /*!
            \return Nothing. Function is void.
          */
    void on_helpButton_clicked();

private:
    //! A private variable.
         /*!
           Pointer that points to the user interface of the view purchases window
         */
    Ui::AdminViewPurchases *ui;
};

#endif // ADMINVIEWPURCHASES_H
