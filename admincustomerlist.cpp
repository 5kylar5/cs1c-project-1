/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "admincustomerlist.h"
#include "ui_admincustomerlist.h"
#include "adminportal.h"
#include "customerlist.h"
#include <QMessageBox>
#include <QDebug>


AdminCustomerList::AdminCustomerList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminCustomerList)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}


AdminCustomerList::~AdminCustomerList()
{
    delete ui;
}

void AdminCustomerList::on_backButton_clicked()
{
    AdminPortal *adminPortal = new AdminPortal();
    adminPortal -> show();
    this -> close();
}

void AdminCustomerList::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This page allows the admin to view the entire customer list from the database.");
        msgBox.exec();
}

void AdminCustomerList::on_comboBox_currentIndexChanged(int index)
{
    switch (index)
    {
        case 0:
        ui->customerList->SortByName();
        break;

        case 1:
        ui->customerList->SortByKey();
        break;
    }
}
