/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "loginscreen.h"
#include "pamphlet.h"

#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pamphletButton_clicked()
{
    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void MainWindow::on_adminButton_clicked()
{
    LoginScreen *login = new LoginScreen();
    login -> show();
    this -> close();
}

void MainWindow::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This is the Home Screen to the iCyberSecurity Application. Press \"See Pamphlet\" if you are a customer or \"Login\" if you are an administrator");
        msgBox.exec();
}
