/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef CONTACT_H
#define CONTACT_H

#include <QWidget>

/*! \namespace Ui
    \brief class Contact.

    This namespace contains the class Contact.
*/
namespace Ui {
class Contact;
}

//!  Contact class.
/*!
  This class contains all the public and private data members/methods associated with the Contact Form.
*/
class Contact : public QWidget
{
    Q_OBJECT

public:

    //! Contact constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit Contact(QWidget *parent = nullptr);

    //! Contact destructor.
        /*!
         This destructs the contact form UI.
        */
    ~Contact();

private slots:

    //! Allows the user to return to the previous window.
       /*!
         \return Nothing. Function is void.
       */
    void on_backButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! Sends the contact page data.
       /*!
         \return Nothing. Function is void.
       */
    void on_sendButton_clicked();

    //! Error checks the inputted data.
       /*!
         \return Nothing. Function is void.
       */
    void errorCheck();

private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the contact window
      */
    Ui::Contact *ui;

    //! A private variable.
      /*!
        Allows the errorCheck() function to properly.
        \sa errorCheck()
      */
    bool badInput = false;
};

#endif // CONTACT_H
