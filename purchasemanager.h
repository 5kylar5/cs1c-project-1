/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef PURCHASEMANAGER_H
#define PURCHASEMANAGER_H

#include "purchase.h"
#include <QString>
#include <QSqlDatabase>
#include <QVector>

//!  PurchaseManager class.
/*!
  This class contains all the public and private data members/methods associated with a purchase manager.
*/
class PurchaseManager
{
public:

    //! Purchase manager default constructor.
          /*!
            This constructor sets the data associated with the manager
          */
    PurchaseManager();

    //! PurchaseManager destructor.
        /*!
         This destructs the purchase manager.
        */
    ~PurchaseManager();

    //! Adds a purchase.
        /*!
          \param purchase the purchase
          \return Bool
        */
    bool AddPurchase(Purchase purchase);

    //! Gets the purchase by ID
        /*!
          \param Id
          \return Purchase object
        */
    Purchase GetPurchaseById(int Id);

    //! Gets the purchase by name
        /*!
          \param name
          \return Purchase object
        */
    Purchase GetPurchaseByName(QString name);

    //! Deletes purchase by ID
        /*!
          \param Id
          \return Nothing. The function is void.
        */
    void DeletePurchaseById(int Id);

    //! Gets the list
        /*!
          \param list
          \param size
          \return Nothing. The function is void.
        */
    void GetList(Purchase list[], int &size);
private:
    //! A private variable.
      /*!
        The SQL database.
      */
       QSqlDatabase database;

       //! A private variable.
         /*!
           The File path.
         */
       QString FilePath;
};






#endif // PURCHASEMANAGER_H
