/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef TESTIMONIAL_H
#define TESTIMONIAL_H

#include <QWidget>

/*! \namespace Ui
    \brief class Testimonial.

    This namespace contains the class Testimonial.
*/
namespace Ui {
class Testimonial;
}

//!  Testimonial class.
/*!
  This class contains all the public and private data members/methods associated with the Testimonial.
*/
class Testimonial : public QWidget
{
    Q_OBJECT

public:

    //! Testimonial constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit Testimonial(QWidget *parent = nullptr);

    //! Testimonial destructor.
            /*!
             This destructs the Testimonial UI.
            */
    ~Testimonial();

private slots:

    //! Allows the user to return to the main window.
          /*!
            \return Nothing. Function is void.
          */
    void on_backButton_clicked();

    //! Displays the Help for that window.
           /*!
             \return Nothing. Function is void.
           */
    void on_helpButton_clicked();

    //! Submits the testimonial.
           /*!
             \return Nothing. Function is void.
           */
    void on_submitButton_clicked();

    //! Error checks the inputted data.
           /*!
             \return Nothing. Function is void.
           */
    void errorCheck();

private:
    //! A private variable.
          /*!
            Pointer that points to the user interface of the Testimonial window
          */
    Ui::Testimonial *ui;

    //! A private variable.
          /*!
            Allows the errorCheck() function to properly.
            \sa errorCheck()
          */
    bool badInput = false;
};

#endif // TESTIMONIAL_H
