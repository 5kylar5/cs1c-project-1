/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "purchasemanager.h"
#include "ckeckout.h"
#include "ui_ckeckout.h"
#include "pamphlet.h"
#include "receipt.h"
#include <QMessageBox>
#include <QDebug>
#include "databasemanager.h"

CkeckOut::CkeckOut(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CkeckOut)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}

CkeckOut::~CkeckOut()
{
    delete ui;
}

void CkeckOut::on_backButton_clicked()
{
    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void CkeckOut::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("To check-out a service, first, please choose the \"Services\" and \"Maintenance Plan\" of your liking. Second, enter your name, address, city, zipcode, and state. Lastly, once your happy with the information you've entered click \"Place Order\". ");
        msgBox.exec();
}


void CkeckOut::on_orderButton_clicked()
{

    errorCheck();

    //Assigns text input fields to local vaiables
    QString name    = ui->name->text();
    QString street  = ui->address->text();
    QString city    = ui->city->text();
    QString state   = ui->state->text();
    QString zipcode = ui->zipcode->text();

    int email = !(ui->checkBox_1->checkState() == Qt::Unchecked);
    int network = !(ui->checkBox_2->checkState() == Qt::Unchecked);
    int cloud = !(ui->checkBox_3->checkState() == Qt::Unchecked);
    int maintence = !(ui->checkBox_4->checkState() == Qt::Unchecked);

    PurchaseManager database;
    DatabaseManager custDb;


   if(badInput == false)
   {

       Purchase purchase;
       Customer cust;
       cust.SetName(name);
       cust.SetAddress(street);
       cust.SetZip(zipcode);
       cust.SetState(state);
       cust.SetCity(city);
       purchase.SetCustomer(cust);

       purchase.SetEmail(email);
       purchase.SetCloud(cloud);
       purchase.SetNetwork(network);
       purchase.SetMaintence(maintence);


       custDb.AddCustomer(cust);
       database.AddPurchase(purchase);

   }

   Purchase completePurchase = database.GetPurchaseByName(name);

   QMessageBox msgBox;
       msgBox.setText("Thank you! Your total for the Purchase was: $" + QString::number(completePurchase.GetPrice()) );
       msgBox.exec();

   if(badInput == false)
   {
       Pamphlet *pamphlet = new Pamphlet();
       pamphlet -> show();
       this -> close();
   }

}


void CkeckOut::errorCheck()
{
    badInput = false;

    if(ui->name->text() == "")
    {
        badInput = true;
        ui->nameLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->address->text() == "")
    {
         badInput = true;
        ui->addressLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->addressLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->city->text() == "")
    {
         badInput = true;
        ui->cityLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->cityLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->zipcode->text() == "")
    {
         badInput = true;
        ui->zipcodeLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->zipcodeLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->state->text() == "")
    {
         badInput = true;
        ui->stateLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->stateLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->checkBox_1->checkState() == Qt::Unchecked && ui->checkBox_2->checkState() == Qt::Unchecked && ui->checkBox_3->checkState() == Qt::Unchecked && ui->checkBox_4->checkState() == Qt::Unchecked )
    {
         badInput = true;
        ui->checkBox_1->setStyleSheet("color: rgb(252, 0, 6);");
        ui->checkBox_2->setStyleSheet("color: rgb(252, 0, 6);");
        ui->checkBox_3->setStyleSheet("color: rgb(252, 0, 6);");
        ui->checkBox_4->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->checkBox_1->setStyleSheet("color: rgb(255, 255, 255);");
        ui->checkBox_2->setStyleSheet("color: rgb(255, 255, 255);");
        ui->checkBox_3->setStyleSheet("color: rgb(255, 255, 255);");
        ui->checkBox_4->setStyleSheet("color: rgb(255, 255, 255);");
    }

}
