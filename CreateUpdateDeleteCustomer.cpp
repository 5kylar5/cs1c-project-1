#include "CreateUpdateDeleteCustomer.h"

CreateUpdateDeleteCustomer::CreateUpdateDeleteCustomer()
{
    head          = new CustomerInfo;
    head          = nullptr;
    tail          = new CustomerInfo;
    tail          = nullptr;
}

void CreateUpdateDeleteCustomer::CreateCustomer()
{
    CustomerInfo *customerInstance = nullptr;
    customerInstance = new CustomerInfo;

    cout << "Enter Customer Name: ";
    getline(cin,customerInstance->name);

    cout << "Enter Customer Address: ";
    getline(cin,customerInstance->address);

    cout << "Enter Customer Index: ";
    cin  >> customerInstance->index;
    cin.ignore(10000,'\n');

    if(head==nullptr&&tail==nullptr)
    {

        head = customerInstance;
        tail = customerInstance;
    }
    else
    {
        tail->next = customerInstance;
        tail = customerInstance;
    }
//     delete customerInstance;
//     cout << " in create " << head->name << endl;
}

void CreateUpdateDeleteCustomer::UpdateCustomer()
{
//	cout << "in update " << head->name << endl;
    CustomerInfo *beginSearch = new CustomerInfo;
    beginSearch = nullptr;
    beginSearch = this->head;
    CustomerInfo indexSearch;
    bool foundIndex = false;

    cout << "Name in update customer: " << beginSearch->name << endl;

    cout << "Enter the Customers Index Number to Update the information (0 to exit): ";
    cin  >> indexSearch.index;
    cin.ignore(10000,'\n');
    cout << endl;

    do
    {
        if(indexSearch.index == 0)
        {
            break;
        }
        while(beginSearch->index!=indexSearch.index && beginSearch!=nullptr)
        {
            beginSearch = beginSearch->next;
        }
        if(beginSearch->index == indexSearch.index)
        {
            foundIndex = true;
            cout << "Found user \n" << "Name: " << beginSearch->name << endl << "Address: "
                 << beginSearch->address << endl << "Index: " << beginSearch->index << endl;
        }
        else
        {
            cout << "***User has not been found Enter 0 for Exit or re-enter a valid index*** ";
            cin  >> indexSearch.index;
            cin.ignore(10000,'\n');
            cout << endl;
        }
    }while(foundIndex==false);

    if(beginSearch->index>0 && foundIndex)
    {
        cout << "Change name to     : ";
        getline(cin,beginSearch->name);

        cout << "Change Address to: ";
        getline(cin,beginSearch->address);
        cout << endl;
    }
//    delete beginSearch;
//    delete indexSearch;
}

void CreateUpdateDeleteCustomer::DeleteCustomer()
{
    CustomerInfo *beginSearch = new CustomerInfo;
    beginSearch = this->head;
    CustomerInfo indexSearch;
    bool foundIndex = false;

    cout << "Enter the Customers Index Number to Delete the information (0 to exit): ";
    cin  >> indexSearch.index;
    cin.ignore(10000,'\n');


    do
    {
        if(indexSearch.index == 0)
        {
            break;
        }

        while(beginSearch->index!=indexSearch.index && beginSearch!=nullptr)
        {
            beginSearch = beginSearch->next;
        }
        if(beginSearch->index == indexSearch.index)
        {
            foundIndex = true;
            cout << "Found user \n" << "Name " << beginSearch->name << endl << "Address: "
                                    << beginSearch->address << endl << "Index: " << beginSearch->index << endl;
        }
        else
        {
            cout << "***User has not been found Enter 0 for Exit or re-enter a valid index*** ";
            cin  >> indexSearch.index;
            cin.ignore(10000,'\n');
        }
    }while(foundIndex==false);

    cout << "Out of do While";
    if(beginSearch->index>0)
    {
        CustomerInfo *temp = new CustomerInfo;
        temp = head;
        cout << temp->name;
        while(temp!=nullptr)
        {
            cout << temp->name;
            if(temp->next->index == beginSearch->index)
            {
                temp->next = beginSearch->next;
                cout << "Index: " << indexSearch.index << " has been deleted\n";
            }
            else
            {
                cout << temp->name;
                temp = temp->next;
            }
        }
    }
    cout << "You have exited the Edit Customer program\n";
}

CreateUpdateDeleteCustomer::~CreateUpdateDeleteCustomer()
{
//	delete head;
//	delete tail; I dont think i have to delete them because u will always need these for the database
    // i think i have to delete the other pointers though since i allocated memory for them

}
