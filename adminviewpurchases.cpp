/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "adminviewpurchases.h"
#include "ui_adminviewpurchases.h"
#include "adminportal.h"
#include "purchaselist.h"
#include <QMessageBox>
#include <QDebug>

AdminViewPurchases::AdminViewPurchases(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminViewPurchases)
{
    ui->setupUi(this);
}

AdminViewPurchases::~AdminViewPurchases()
{
    delete ui;
}

void AdminViewPurchases::on_backButton_clicked()
{
    AdminPortal *adminPortal = new AdminPortal();
    adminPortal -> show();
    this -> close();
}

void AdminViewPurchases::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This page allows the admin to view all purchases.");
        msgBox.exec();
}

