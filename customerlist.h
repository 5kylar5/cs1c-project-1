/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef CUSTOMERLIST_H
#define CUSTOMERLIST_H

#include <QWidget>

/*! \namespace Ui
    \brief class CustomerList.

    This namespace contains the class CustomerList.
*/
namespace Ui {
class CustomerList;
}

//!  CustomerList class.
/*!
  This class contains all the public and private data members/methods associated with the Customer List.
*/
class CustomerList : public QWidget
{
    Q_OBJECT

public:
    //! CustomerList constructor.
        /*!
          This constructor sets up the UI.
        */
    explicit CustomerList(QWidget *parent = nullptr);

    //! CustomerList destructor.
        /*!
         This destructs the Customer List.
        */
    ~CustomerList();

    //! Sorts the customers by name.
        /*!
          \return Nothing. Function is void.
        */
    void SortByName();

    //! Sorts the customers by key.
        /*!
          \return Nothing. Function is void.
        */
    void SortByKey();

private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the customer list window
      */
    Ui::CustomerList *ui;
};

#endif // CUSTOMERLIST_H
