/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef REQUESTPANPHLET_H
#define REQUESTPANPHLET_H

#include <QWidget>
#include <databasemanager.h>
#include <customer.h>
#include <adminportal.h>
#include <adminaddcustomer.h>

/*! \namespace Ui
    \brief class RequestPanphlet.

    This namespace contains the class RequestPanphlet.
*/
namespace Ui {
class RequestPanphlet;
}

//!  RequestPanphlet class.
/*!
  This class contains all the public and private data members/methods associated with the Request Pamphlet Window.
*/
class RequestPanphlet : public QWidget
{
    Q_OBJECT

public:

    //! RequestPanphlet constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit RequestPanphlet(QWidget *parent = nullptr);

    //! RequestPanphlet destructor.
        /*!
         This destructs the request pamphlet UI.
        */
    ~RequestPanphlet();

private slots:

    //! Allows the user to return to the main window.
       /*!
         \return Nothing. Function is void.
       */
    void on_backButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! Places a request for the pamphlet.
       /*!
         \return Nothing. Function is void.
       */
    void on_requestButton_clicked();

    //! Error checks the inputted data.
       /*!
         \return Nothing. Function is void.
       */
    void errorCheck();

private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the request pamphlet window
      */
    Ui::RequestPanphlet *ui;

    //! A private variable.
      /*!
        Allows the errorCheck() function to properly.
        \sa errorCheck()
      */
    bool badInput = false;
};

#endif // REQUESTPANPHLET_H
