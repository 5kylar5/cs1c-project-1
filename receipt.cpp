/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "receipt.h"
#include "ui_receipt.h"
#include "mainwindow.h"
#include <QMessageBox>

Receipt::Receipt(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Receipt)
{
    ui->setupUi(this);
}

Receipt::Receipt(QWidget *parent, QString name) :
    QWidget(parent),
    ui(new Ui::Receipt)
{
    ui->setupUi(this);
    ui->name->text() = name;
}

Receipt::~Receipt()
{
    delete ui;
}

void Receipt::on_homeButton_clicked()
{
    MainWindow *main = new MainWindow();
    main -> show();
    this -> close();
}

void Receipt::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("PLS ADD HELP MSG");
        msgBox.exec();
}
