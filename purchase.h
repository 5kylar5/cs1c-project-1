/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef PURCHASE_H
#define PURCHASE_H

#include "customer.h"

const int PRICES[4] {420,670,969,75};

const QString SERVICES[4] {"Email Security","Network Security",
                           "Cloud Security","Monthly Maintence Plan"};

//!  Purchase class.
/*!
  This class contains all the public and private data members/methods associated with a purchase.
*/
class Purchase
{
    public:

    //! Purchase default constructor.
          /*!
            This constructor sets Purchases to 0
          */
        Purchase();

        //! Purchase non default constructor.
              /*!
                This constructor sets private data memebers to their associated parameters.
                \param customer the customer
                \param email the email service
                \param network the network service
                \param cloud the cloud service
                \param maintence the maintence service plan
              */
        Purchase(class Customer customer, int email, int network, int cloud, int maintence);

        //! Purchase destructor.
            /*!
             This destructs the purchase object.
            */
        ~Purchase();

        //! Sets the customers.
            /*!
              \param customer the customer
              \return Nothing. Function is void.
            */
        void SetCustomer(class Customer customer);

        //! Sets the email service.
            /*!
              \param email the email service
              \return Nothing. Function is void.
            */
        void SetEmail(int email);

        //! Sets the network service.
            /*!
              \param network the network service
              \return Nothing. Function is void.
            */
        void SetNetwork(int network);

        //! Sets the cloud service.
            /*!
              \param cloud the cloud service
              \return Nothing. Function is void.
            */
        void SetCloud(int cloud);

        //! Sets the maintence service.
            /*!
              \param maintence the maintence service
              \return Nothing. Function is void.
            */
        void SetMaintence(int maintence);

        //! Sets the ID.
            /*!
              \param id the ID
              \return Nothing. Function is void.
            */
        void SetId(int id);

        //! Gets Plan to string.
            /*!
              \return nothing.
            */
        void BuildPlanString();

        //! Gets Total Price.
            /*!
              \return nothing.
            */
        void GetTotalPrice();

        //! Gets ID.
            /*!
              \return nothing.
            */
        int GetId();

        //! Gets Customer data.
            /*!
              \return Customer.
            */
        Customer GetCustomer();

        //! Gets email plan.
            /*!
              \return int.
            */
        int GetEmail();

        //! Gets network plan.
            /*!
              \return int.
            */
        int GetNetwork();

        //! Gets cloud plan.
            /*!
              \return int.
            */
        int GetCloud();

        //! Gets maintence plan.
            /*!
              \return int.
            */
        int GetMaintence();

        //! Gets plan.
            /*!
              \return QString.
            */
        QString GetPlans();

        //! Gets price.
            /*!
              \return int.
            */
        int GetPrice();

    private:

        //! A private variable.
          /*!
            ID of customer
          */
        int Id;

        //! A private variable.
          /*!
            Customer object
          */
        Customer Customer;

        //! A private variable.
          /*!
            email selected state
          */
        int EmailSelected;

        //! A private variable.
          /*!
            network selected state
          */
        int NetworkSelected;

        //! A private variable.
          /*!
            cloud selected state
          */
        int CloudSelected;

        //! A private variable.
          /*!
            maintence selected state
          */
        int MaintenceSelected;

        //! A private variable.
          /*!
            all services in a string
          */
        QString PlanString;

        //! A private variable.
          /*!
            total price of all selected services
          */
        int TotalPrice;


};

#endif // PURCHASE_H
