#-------------------------------------------------
#
# Project created by QtCreator 2019-02-03T16:10:07
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iCyberSecurity
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    loginscreen.cpp \
    pamphlet.cpp \
    contact.cpp \
    testimonial.cpp \
    requestpanphlet.cpp \
    adminportal.cpp \
    adminaddcustomer.cpp \
    databasemanager.cpp \
    customerlist.cpp \
    admincustomerlist.cpp \
    adminremovecustomer.cpp \
    admineditcustomer.cpp \
    adminviewpurchases.cpp \
    purchaselist.cpp \
    receipt.cpp \
    customer.cpp \
    Ckeckout.cpp \
    testimonialmanager.cpp \
    testimonialcalss.cpp \
    purchase.cpp \
    purchasemanager.cpp

HEADERS += \
        mainwindow.h \
    loginscreen.h \
    pamphlet.h \
    contact.h \
    testimonial.h \
    requestpanphlet.h \
    adminportal.h \
    adminaddcustomer.h \
    databasemanager.h \
    customerlist.h \
    admincustomerlist.h \
    adminremovecustomer.h \
    admineditcustomer.h \
    adminviewpurchases.h \
    purchaselist.h \
    receipt.h \
    customer.h \
    ckeckout.h \
    testimonialmanager.h \
    testimonialcalss.h \
    purchase.h \
    purchasemanager.h

FORMS += \
        mainwindow.ui \
    loginscreen.ui \
    pamphlet.ui \
    contact.ui \
    testimonial.ui \
    requestpanphlet.ui \
    ckeckout.ui \
    adminportal.ui \
    adminaddcustomer.ui \
    customerlist.ui \
    admincustomerlist.ui \
    adminremovecustomer.ui \
    admineditcustomer.ui \
    adminviewpurchases.ui \
    purchaselist.ui \
    receipt.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    assets.qrc
