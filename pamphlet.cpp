/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "pamphlet.h"
#include "ui_pamphlet.h"
#include "mainwindow.h"
#include "contact.h"
#include "testimonial.h"
#include "requestpanphlet.h"
#include "ckeckout.h"
#include <QMessageBox>
#include "databasemanager.h"
#include "testimonialmanager.h"
#include "testimonialcalss.h"
#include "testimonial.h"
#include <QDebug>

Pamphlet::Pamphlet(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Pamphlet)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());

    bool valid = true;
    TestimonialCalss list[100];
    int size = -1;
    int *sizePtr = &size;


    if(valid)
    {
        TestimonialManager tManager;
        tManager.GetList(list, size);
    }


    int index = 0;

    if(*sizePtr == -1)
    {
        return;
    }
    QString name;
    QString comment;
    Customer cust;

    DatabaseManager db;

    while(index <= *sizePtr)
    {
        //cust = list[index].GetCustomer();

        qDebug() << list[index].GetId();

        if(list[index].GetId() != 0 && list[index].GetText() != "" && list[index].GetId() < 100)
        {
            if(list[index].GetCustomer().GetId() == 0)
            {
                name = "Anonymous";
            }
            else
            {
                cust = db.GetCustomerById(list[index].GetCustomer().GetId());
                name = cust.GetName();

            }

            comment = list[index].GetText();

            ui->testamonialList->addItem(" \" " + comment + "\" -" + name);

        }
         index++;

    }
}

Pamphlet::~Pamphlet()
{
    delete ui;
}

void Pamphlet::on_homeButton_clicked()
{
    MainWindow *main = new MainWindow();
    main -> show();
    this -> close();
}

void Pamphlet::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("The pamphlet explains what iCyberSecurity has to offer. Displays a description of our firm, key selling points, etc.");
        msgBox.exec();
}



void Pamphlet::on_contactButton_clicked()
{
    Contact *contact = new Contact();
    contact -> show();
    this -> close();
}

void Pamphlet::on_requestCopyButton_clicked()
{
       RequestPanphlet *request = new RequestPanphlet;
       request -> show();
       this -> close();
}

void Pamphlet::on_checkOutButton_clicked()
{

    CkeckOut *checkout = new CkeckOut;
    checkout -> show();
    this -> close();
}

void Pamphlet::on_testimonialButton_clicked()
{
    Testimonial *testimonial = new Testimonial();
    testimonial -> show();
    this -> close();
}
