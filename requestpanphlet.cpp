/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "requestpanphlet.h"
#include "ui_requestpanphlet.h"
#include "pamphlet.h"
#include <QMessageBox>

RequestPanphlet::RequestPanphlet(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RequestPanphlet)
{
    ui->setupUi(this);
}

RequestPanphlet::~RequestPanphlet()
{
    delete ui;
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}

void RequestPanphlet::on_backButton_clicked()
{
    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void RequestPanphlet::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("If you'd like to recieve a pamphlet from us, please enter your name with the corresponding details below it. Lastly, click \"Request\".");
        msgBox.exec();
}


void RequestPanphlet::on_requestButton_clicked()
{
    QString nameF, addressF, cityF, stateF;
    QString zipcodeF;



    nameF    = this->ui -> name->text();
    addressF = this->ui -> address->text();
    cityF    = this->ui -> city->text();
    stateF   = this->ui -> state->text();
    zipcodeF = this->ui -> zipcode->text();


    errorCheck();

    if(badInput == false)
    {
        DatabaseManager database;

        Customer existingCustomer = database.GetCustomerByName(nameF);

        if(existingCustomer.GetId() != 0){
            existingCustomer.SetPamphlet(1);
            database.UpdateCustomer(existingCustomer);
        }
        else {
            Customer customer;
            customer.SetName(nameF);
            customer.SetAddress(addressF);
            customer.SetCity(cityF);
            customer.SetZip(zipcodeF);
            customer.SetState(stateF);
            customer.SetItrest("Request Pamphlet");
            customer.SetImportance("key");
            customer.SetPamphlet(1);

            database.AddCustomer(customer);
        }



       Pamphlet *pamphlet = new Pamphlet();
       pamphlet -> show();
       this -> close();

    }

}

void RequestPanphlet::errorCheck()
{
    badInput = false;

    if(ui->name->text() == "")
    {
        badInput = true;
        ui->nameLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->address->text() == "")
    {
         badInput = true;
        ui->addressLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->addressLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->city->text() == "")
    {
         badInput = true;
        ui->cityLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->cityLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->zipcode->text() == "")
    {
         badInput = true;
        ui->zipcodeLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->zipcodeLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->state->text() == "")
    {
         badInput = true;
        ui->stateLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->stateLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

}
