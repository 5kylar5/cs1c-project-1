/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef ADMINCUSTOMERLIST_H
#define ADMINCUSTOMERLIST_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminCustomerList.

    This namespace contains the class AdminCustomerList.
*/
namespace Ui {
class AdminCustomerList;
}

//!  AdminCustomerList class.
/*!
  This class contains all the public and private data members/methods associated with the Customer List Form within the admin portal.
*/
class AdminCustomerList : public QWidget
{
    Q_OBJECT

public:
    //! AdminCustomerList constructor.
          /*!
            This constructor sets up the UI.
          */
    explicit AdminCustomerList(QWidget *parent = nullptr);

    //! AdminCustomerList destructor.
            /*!
             This destructs the customer list UI.
            */
    ~AdminCustomerList();

private slots:
    //! Allows the user to return to the admin portal.
        /*!
          \return Nothing. Function is void.
        */
    void on_backButton_clicked();

    //! Displays the Help for that window.
           /*!
             \return Nothing. Function is void.
           */
    void on_helpButton_clicked();

    //! When the comboBox index is changed.
           /*!
             \return Nothing. Function is void.
           */
    void on_comboBox_currentIndexChanged(int index);

private:

    //! A private variable.
       /*!
         Pointer that points to the user interface of the customer list window
       */
    Ui::AdminCustomerList *ui;
};

#endif // ADMINCUSTOMERLIST_H
