/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "purchaselist.h"
#include "ui_purchaselist.h"
#include "purchasemanager.h"


PurchaseList::PurchaseList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PurchaseList)
{
    ui->setupUi(this);
    ui->purchaseList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QStringList columnTitles;
    ui->purchaseList->setColumnCount(3);
    columnTitles << "Name" << "Product" << "Amount";
    ui->purchaseList->setHorizontalHeaderLabels(columnTitles);

    ui->purchaseList->verticalHeader()->setVisible(false); // Removes first vertical column

    Purchase list[100];
    int size = -1;
    PurchaseManager db;
    db.GetList(list,size);

    int index = 0;

    if(size == -1)
    {
        return;
    }
    ui->purchaseList->setRowCount(size);
    while(index <= size)
    {
        if(list[index].GetId() != 0)
        {
             ui->purchaseList->setItem(index,0,new QTableWidgetItem(list[index].GetCustomer().GetName()));
             ui->purchaseList->setItem(index,1,new QTableWidgetItem(list[index].GetPlans()));
             ui->purchaseList->setItem(index,2,new QTableWidgetItem("$" + QString::number(list[index].GetPrice())));

        }

         index++;
    }
}

PurchaseList::~PurchaseList()
{
    delete ui;

}
