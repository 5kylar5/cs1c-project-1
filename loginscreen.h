/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H

#include <QWidget>

/*! \namespace Ui
    \brief class LoginScreen.

    This namespace contains the class LoginScreen.
*/
namespace Ui {
class LoginScreen;
}

//!  LoginScreen class.
/*!
  This class contains all the public and private data members/methods associated with the admin login screen.
*/
class LoginScreen : public QWidget
{
    Q_OBJECT

public:

    //! LoginScreen constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit LoginScreen(QWidget *parent = nullptr);

    //! LoginScreen destructor.
         /*!
          This destructs the login screen UI.
         */
    ~LoginScreen();

private slots:

    //! Allows the user to return to the main screen.
       /*!
         \return Nothing. Function is void.
       */
    void on_homeButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! If password enter is pressed.
         /*!
           \return Nothing. Function is void.
         */
    void on_password_returnPressed();

    //! Logs in with inputted credentials.
         /*!
           \return Nothing. Function is void.
         */
    void on_loginButton_clicked();

private:

    //! A private variable.
      /*!
        Pointer that points to the user interface of the login screen window
      */
    Ui::LoginScreen *ui;
};

#endif // LOGINSCREEN_H
