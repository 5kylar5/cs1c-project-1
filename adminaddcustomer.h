/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef ADMINADDCUSTOMER_H
#define ADMINADDCUSTOMER_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminAddCustomer.

    This namespace contains the class AdminAddCustomer.
*/
namespace Ui {
class AdminAddCustomer;
}

//!  AdminAddCustomer class.
/*!
  This class contains all the public and private data members/methods associated with the Add Customer Form within the admin portal.
*/
class AdminAddCustomer : public QWidget
{
    Q_OBJECT

public:
    //! AdminAddCustomer constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit AdminAddCustomer(QWidget *parent = nullptr);

    //! AdminAddCustomer destructor.
        /*!
         This destructs the add customer UI.
        */
    ~AdminAddCustomer();

private slots:
    //! Adds a customer to the database.
       /*!
         \return Nothing. Function is void.
       */
    void on_addButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();


    //! Allows the user to return to the admin portal.
       /*!
         \return Nothing. Function is void.
       */
    void on_backButton_clicked();

    //! Error checks the inputted data.
       /*!
         \return Nothing. Function is void.
       */
    void errorCheck();



private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the add customer window
      */
    Ui::AdminAddCustomer *ui;

    //! A private variable.
      /*!
        Allows the errorCheck() function to properly.
        \sa errorCheck()
      */
    bool badInput = false;
};

#endif // ADMINADDCUSTOMER_H
