#pragma once

#include <iostream>
#include <string>

using namespace std;

class CreateUpdateDeleteCustomer
{
        public:
            CreateUpdateDeleteCustomer();
            ~CreateUpdateDeleteCustomer();

//            CustomerInfo GetCustomerInfo();
            void CreateCustomer(); //Returns the index of the customer
            void UpdateCustomer(); //use customerIndex to access the customers info
            void DeleteCustomer();//use customerIndex to access the customers info and probably
                                                  //use linked list to be able to delete a node

        private:
            struct CustomerInfo
                    {
                        string name;
                        string address;
                        int    index;
                        CustomerInfo* next;
                    };
            CustomerInfo *head,*tail;

};



#endif /* CREATEUPDATEDELETECUSTOMER_H_ */
