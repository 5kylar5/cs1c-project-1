#ifndef TESTIMONIALMANAGER_H
#define TESTIMONIALMANAGER_H

#include "testimonialcalss.h"
#include "customer.h"
#include <QString>
#include <QSqlDatabase>
#include <QVector>

class TestimonialManager
{
public:
    TestimonialManager();
    //! DatabaseManager destructor.
        /*!
         This destructs the DatabaseManager.
        */
    ~TestimonialManager();

    //! Adds a testimonial to the database.
       /*!
         \param testimonial the testimonial
         \return Bool
       */
    bool AddTestimonial(TestimonialCalss testimonial);


    //! Gets testimonial list
       /*!
         \param list the list of testimonial objects
         \param size the size of list
         \return nothing
       */
    void GetList(TestimonialCalss list[], int &size);


private:
    //! A private variable.
      /*!
        The SQL database.
      */
       QSqlDatabase database;

       //! A private variable.
         /*!
           The File path.
         */
       QString FilePath;
};

#endif // TESTIMONIALMANAGER_H
