/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "adminremovecustomer.h"
#include "ui_adminremovecustomer.h"
#include "adminportal.h"
#include <QMessageBox>
#include "databasemanager.h"
#include <QIntValidator>

AdminRemoveCustomer::AdminRemoveCustomer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminRemoveCustomer)
{
    ui->setupUi(this);
    ui->id->setValidator( new QIntValidator(0, 100, this) );

    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());}


AdminRemoveCustomer::~AdminRemoveCustomer()
{
    delete ui;
}

void AdminRemoveCustomer::on_backButton_clicked()
{
    AdminPortal *adminPortal = new AdminPortal();
    adminPortal -> show();
    this -> close();
}

void AdminRemoveCustomer::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This form allows the admin to remove a customer from the database. Please enter an ID to remove.");
        msgBox.exec();
}

void AdminRemoveCustomer::on_removeButton_clicked()
{
    QString idString    = ui->id->text();
    int id = idString.split(" ")[0].toInt();

    errorCheck();

    if(badInput == false)
    {

        DatabaseManager db;
        db.DeleteCustomerById(id);

        QMessageBox::StandardButton reply = QMessageBox::question(this, "Remove Customer", "Success! Would you like to remove another customer?",
                                                                  QMessageBox::Yes | QMessageBox::No);
        if(reply == QMessageBox::No)
        {
            AdminPortal *adminPortal = new AdminPortal();
            adminPortal -> show();
            this -> close();
        }
        else
        {
            AdminRemoveCustomer *adminRemove = new AdminRemoveCustomer();
            adminRemove -> show();
            this -> close();
        }

    }

}

void AdminRemoveCustomer::errorCheck()
{
    badInput = false;

    if(ui->id->text() == "")
    {
        badInput = true;
        ui->idLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->idLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }
}


