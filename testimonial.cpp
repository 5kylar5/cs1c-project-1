/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "testimonialmanager.h"
#include "databasemanager.h"
#include "testimonial.h"
#include "ui_testimonial.h"
#include "pamphlet.h"
#include <QMessageBox>
#include <QDebug>

Testimonial::Testimonial(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Testimonial)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());

    ui->customerComboBox->addItem("Anonymous");

    Customer list[100];
    int size = -1;
    DatabaseManager db;
    db.GetList(list,size);

    int index = 0;

    while(index <= size)
    {
         if(list[index].GetName().trimmed() != "")
            ui->customerComboBox->addItem(list[index].GetName());
         index++;
    }


}

Testimonial::~Testimonial()
{
    delete ui;
}

void Testimonial::on_backButton_clicked()
{
    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void Testimonial::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("To add a testimonial, click the text box below \"Enter Testimonial:\", then simply add your comment and click \"Submit\". ");
        msgBox.exec();
}


void Testimonial::on_submitButton_clicked()
{
    errorCheck();



    QString name = ui->customerComboBox->currentText();
    QString text = ui->review->toPlainText();
    QString anon = "Anonymous";
    int custId = 0;
    Customer cust;

    if(name != anon)
    {
        DatabaseManager db;
        cust = db.GetCustomerByName(name);
        custId = cust.GetId();
    }

    TestimonialManager db;

    TestimonialCalss testimonial(cust,0,text);

    db.AddTestimonial(testimonial);

    TestimonialCalss list[100];
    int size = -1;
    db.GetList(list,size);

    int index = 0;

    while(index <= size)
    {
         if(list[index].GetId() != 0)
         {
             qDebug() << "Text: " << list[index].GetText();
             qDebug() << "cust name: " << list[index].GetCustomer().GetName();
             qDebug() << "test id: " << list[index].GetId();
             index++;
         }
    }

    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void Testimonial::errorCheck()
{
    badInput = false;

    if(ui->review->toPlainText() == "")
    {
        badInput = true;
        ui->testimonialLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->testimonialLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->customerComboBox->currentIndex()==-1)
    {
         badInput = true;
        ui->customerLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->customerLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }


}
