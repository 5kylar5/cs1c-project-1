/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "admineditcustomer.h"
#include "ui_admineditcustomer.h"
#include "adminportal.h"
#include "customerlist.h"
#include <QMessageBox>
#include "databasemanager.h"

AdminEditCustomer::AdminEditCustomer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminEditCustomer)
{
    ui->setupUi(this);
    ui->id->setValidator( new QIntValidator(0, 100, this) );
    ui->receivedPamphletPicker->setEnabled(false);
    ui->interestLevelPicker->setEnabled(false);
    ui->importancePicker->setEnabled(false);
    ui->saveButton->setEnabled(false);
    ui->saveButton_2->setEnabled(false);


}

AdminEditCustomer::~AdminEditCustomer()
{
    delete ui;
}

void AdminEditCustomer::on_backButton_clicked()
{
    AdminPortal *adminPortal = new AdminPortal();
    adminPortal -> show();
    this -> close();
}

void AdminEditCustomer::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This page allows the admin to edit a customers information. Enter an ID and edit the pre-existing customers data.");
        msgBox.exec();
}

void AdminEditCustomer::on_saveButton_clicked()
{
    //Assigns text input fields to local vaiables

    QString idString    = ui->id->text();
    int id = idString.split(" ")[0].toInt();

    QString name    = ui->name->text();
    QString street  = ui->address->text();
    QString city    = ui->city->text();
    QString state   = ui->state->text();
    QString zipcode = ui->zipcode->text();

    //Concatentates address fields to make address a single string
    QString address = street + " " + city + " " + state + " " + zipcode;

    //Grabs the text form the three pickers
    QString intrestLvl = ui->interestLevelPicker->itemText(ui->interestLevelPicker->currentIndex());
    QString importance = ui->importancePicker->itemText(ui->importancePicker->currentIndex());
    QString received   = ui->receivedPamphletPicker->itemText(ui->receivedPamphletPicker->currentIndex());

    int pamphlet = 0;
    if(received == "yes")
        pamphlet = 1;

    //ADD CUSTOMER TO DATABASE

    //ADD A CUSTOMER FIRST

    errorCheck();

    if(badInput == false)
    {

        DatabaseManager database;

        Customer customer;

        customer.SetId(id);

        customer.SetAddress(street);
        customer.SetZip(zipcode);
        customer.SetState(state);
        customer.SetCity(city);
        customer.SetName(name);
        customer.SetItrest(intrestLvl);
        customer.SetImportance(importance);
        customer.SetPamphlet(pamphlet);

        database.UpdateCustomer(customer);

        AdminPortal *adminPortal = new AdminPortal();
        adminPortal -> show();
        this -> close();
    }




}


void AdminEditCustomer::errorCheck()
{
    badInput = false;

    if(ui->id->text() == "")
    {
        badInput = true;
        ui->idLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->name->text() == "")
    {
        badInput = true;
        ui->nameLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->address->text() == "")
    {
         badInput = true;
        ui->addressLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->addressLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->city->text() == "")
    {
         badInput = true;
        ui->cityLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->cityLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->zipcode->text() == "")
    {
         badInput = true;
        ui->zipcodeLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->zipcodeLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->state->text() == "")
    {
         badInput = true;
        ui->stateLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->stateLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->receivedPamphletPicker->currentIndex()==-1)
    {
         badInput = true;
        ui->receivedPamphletLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->receivedPamphletLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->importancePicker->currentIndex()==-1)
    {
         badInput = true;
        ui->importanceLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->importanceLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->interestLevelPicker->currentIndex()==-1)
    {
         badInput = true;
        ui->interestLevelLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->interestLevelLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

}

void AdminEditCustomer::on_id_textEdited(const QString &arg1)
{
    ui->name->setReadOnly(false);
    ui->address->setReadOnly(false);
    ui->city->setReadOnly(false);
    ui->zipcode->setReadOnly(false);
    ui->state->setReadOnly(false);
    ui->importancePicker->setEnabled(true);
    ui->interestLevelPicker->setEnabled(true);
    ui->receivedPamphletPicker->setEnabled(true);
    ui->saveButton->setEnabled(true);
    ui->saveButton_2->setEnabled(true);



}



void AdminEditCustomer::on_saveButton_2_clicked()
{

    Customer list[100];
    int size = 0;
    DatabaseManager db;
    db.GetList(list,size);

    QString bound = ui->id->text();
    int boundNum;
    boundNum = bound.toInt();

    if(ui->id->text() != "" && boundNum > 0 && boundNum < 101)
    {
        QString num = ui->id->text();
        int index;
        index = -1;

        for(int x = 1; x<100; x++)
        {
            if(list[0].GetId() == x)
            {
                index = num.toInt() - x;
            }
        }

        if(index != -1)
        {
            ui->name->setText(list[index].GetName());
            ui->city->setText(list[index].GetCity());
            ui->address->setText(list[index].GetAddress());
            ui->zipcode->setText(list[index].GetZip());
            ui->state->setText(list[index].GetState());

            QString textToFind;
            int val;
            QString pamphletStatus;

            textToFind= list[index].GetImportance();
            val = ui->importancePicker->findText(textToFind);
            ui->importancePicker->setCurrentIndex(val);

            textToFind= list[index].GetIntrest();
            val = ui->interestLevelPicker->findText(textToFind);
            ui->interestLevelPicker->setCurrentIndex(val);

            pamphletStatus = "no";
            if(list[index].GetPamphlet())
            {
                pamphletStatus = "yes";
            }
            textToFind = pamphletStatus;
            val = ui->receivedPamphletPicker->findText(textToFind);
            ui->receivedPamphletPicker->setCurrentIndex(val);

        }
    }

}
