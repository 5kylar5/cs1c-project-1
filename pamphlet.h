/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef PAMPHLET_H
#define PAMPHLET_H

#include <QWidget>

/*! \namespace Ui
    \brief class Pamphlet.

    This namespace contains the class Pamphlet.
*/
namespace Ui {
class Pamphlet;
}

//!  Pamphlet class.
/*!
  This class contains all the public and private data members/methods associated with the Pamphlet window.
*/
class Pamphlet : public QWidget
{
    Q_OBJECT

public:

    //! Pamphlet constructor.
        /*!
          This constructor sets up the UI.
        */
    explicit Pamphlet(QWidget *parent = nullptr);

    //! Pamphlet destructor.
        /*!
         This destructs the Pamphlet UI.
        */
    ~Pamphlet();

private slots:

    //! Allows the user to return to the main window.
       /*!
         \return Nothing. Function is void.
       */
    void on_homeButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! Opens the contact us window.
       /*!
         \return Nothing. Function is void.
       */
    void on_contactButton_clicked();

    //! Opens the page that allows a customer to request a copy of the pamphlet.
       /*!
         \return Nothing. Function is void.
       */
    void on_requestCopyButton_clicked();

    //! Opens the service checkout window.
       /*!
         \return Nothing. Function is void.
       */
    void on_checkOutButton_clicked();

    //! Allows a customer to make a testimonial.
       /*!
         \return Nothing. Function is void.
       */
    void on_testimonialButton_clicked();

private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the Pamphlet window
      */
    Ui::Pamphlet *ui;
};

#endif // PAMPHLET_H
