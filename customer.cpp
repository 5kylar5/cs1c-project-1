/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "customer.h"

Customer::Customer()
{
    Id = 0;
}

Customer::Customer(QString name, QString address, QString city,
         QString state, QString zip, QString importance,
         QString intrest, int pamphlet)
{
    Name = name;
    Address = address;
    City = city;
    State = state;
    ZipCode = zip;
    ImportanceLevel = importance;
    IntrestLevel = intrest;
    Pamphlet = pamphlet;
}

Customer::Customer(const Customer& customer)
{
    this->Id = customer.Id;
    this->Name = customer.Name;
    this->Address = customer.Address;
    this->City = customer.City;
    this->State = customer.State;
    this->ZipCode = customer.ZipCode;
    this->ImportanceLevel = customer.ImportanceLevel;
    this->IntrestLevel = customer.IntrestLevel;
    this->Pamphlet = customer.Pamphlet;
}

Customer::~Customer()
{

}

void Customer::SetId(int id)
{
    Id = id;
}
void Customer::SetName(QString name)
{
    Name = name;
}
void Customer::SetAddress(QString address)
{
    Address = address;
}
void Customer::SetCity(QString city)
{
    City = city;
}
void Customer::SetState(QString state)
{
    State = state;
}
void Customer::SetZip(QString zip)
{
    ZipCode = zip;
}
void Customer::SetImportance(QString importance)
{
    ImportanceLevel = importance;
}
void Customer::SetItrest(QString intrest)
{
    IntrestLevel = intrest;
}
void Customer::SetPamphlet(int pamphlet)
{
    Pamphlet = pamphlet;
}

int Customer::GetId()
{
    return Id;
}
QString Customer::GetName()
{
    return Name;
}
QString Customer::GetAddress()
{
    return Address;
}
QString Customer::GetCity()
{
    return City;
}
QString Customer::GetState()
{
    return State;
}
QString Customer::GetZip()
{
    return ZipCode;
}
QString Customer::GetImportance()
{
    return ImportanceLevel;
}
QString Customer::GetIntrest()
{
    return IntrestLevel;
}
int Customer::GetPamphlet()
{
    return Pamphlet;
}
