#include "testimonialmanager.h"
#include "customer.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QMessageBox>

TestimonialManager::TestimonialManager()
{
    QDir directory;


    FilePath = directory.currentPath() + "/../iCyberSecurity/database.db";


    database = QSqlDatabase::addDatabase("QSQLITE");


    database.setDatabaseName(FilePath);

    if(!database.open()) {
        FilePath = directory.currentPath() + "/../../../../iCyberSecurity/database.db";
        database.setDatabaseName(FilePath);
        if(!database.open())
        {
            qDebug() << "Error Connecting Database";
            return;
        }
    }

    qDebug() << "Database Connection was Successful";
      QSqlQuery query;
      if(!query.exec("CREATE TABLE IF NOT EXISTS Testimonial (id INTEGER PRIMARY KEY UNIQUE, CustomerId INT(0), Text VARACHAR);"))
      {
           qWarning() << "ERROR: " << query.lastError().text();
      }

}

TestimonialManager::~TestimonialManager() {
    if(this->database.open()) {
        qDebug() << "Database Connection has been Closed";

        database.close();
    }
}

 void TestimonialManager::GetList(TestimonialCalss list[], int &size)
 {

     QSqlQuery query;

     if(!query.exec("SELECT * from Testimonial"))
     {
         qWarning() << "ERROR: " << query.lastError().text();

     }

     size = 0;

     while(query.next() && size < 100)
     {
          TestimonialCalss newTestimonial;

          newTestimonial.SetId(query.value(0).toInt());

          Customer cust;
          cust.SetId((query.value(1).toInt()));

          newTestimonial.SetCustomer(cust);

          newTestimonial.SetText(query.value(2).toString());

          qDebug() << "Text in Loop" << query.value(2).toString();

          list[size] = newTestimonial;
          size++;
     }
 }

bool TestimonialManager::AddTestimonial(TestimonialCalss testimonial)
{
    QSqlQuery query;
    QString valueString= "(";

    qDebug() << "Method test Text: " << testimonial.GetText();
    qDebug() << "Method test cust name: " << testimonial.GetCustomer().GetName();
    qDebug() << "Method test test id: " << testimonial.GetId();
    valueString += QString::number(testimonial.GetCustomer().GetId()) + ",'";
    valueString += testimonial.GetText() +  "');";

    if(!query.exec("INSERT INTO Testimonial (CustomerId,Text) "
                   "VALUES " + valueString))
    {
        qWarning() << "ERROR: " << query.lastError().text();
        return false;
    }

    return true;
}

