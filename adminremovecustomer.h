/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef ADMINREMOVECUSTOMER_H
#define ADMINREMOVECUSTOMER_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminRemoveCustomer.

    This namespace contains the class AdminRemoveCustomer.
*/
namespace Ui {
class AdminRemoveCustomer;
}

//!  AdminRemoveCustomer class.
/*!
  This class contains all the public and private data members/methods associated with the Remove Customer Form within the admin portal.
*/
class AdminRemoveCustomer : public QWidget
{
    Q_OBJECT

public:
    //! AdminRemoveCustomer constructor.
           /*!
             This constructor sets up the UI.
           */
    explicit AdminRemoveCustomer(QWidget *parent = nullptr);

    //! AdminRemoveCustomer destructor.
            /*!
             This destructs the remove customer UI.
            */
    ~AdminRemoveCustomer();

private slots:

    //! Allows the user to return to the admin portal.
           /*!
             \return Nothing. Function is void.
           */
    void on_backButton_clicked();

    //! Displays the Help for that window.
           /*!
             \return Nothing. Function is void.
           */
    void on_helpButton_clicked();

    //! Removes the customer from the database.
           /*!
             \return Nothing. Function is void.
           */
    void on_removeButton_clicked();

    //! Error checks the inputted data.
           /*!
             \return Nothing. Function is void.
           */
     void errorCheck();

private:
    //! A private variable.
          /*!
             Pointer that points to the user interface of the remove customer window
          */
    Ui::AdminRemoveCustomer *ui;

    //! A private variable.
          /*!
            Allows the errorCheck() function to properly.
            \sa errorCheck()
          */
    bool badInput = false;
};

#endif // ADMINREMOVECUSTOMER_H
