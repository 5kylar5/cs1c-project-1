/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef CKECKOUT_H
#define CKECKOUT_H

#include <QWidget>

/*! \namespace Ui
    \brief class CkeckOut.

    This namespace contains the class CkeckOut.
*/
namespace Ui {
class CkeckOut;
}

//!  CkeckOut class.
/*!
  This class contains all the public and private data members/methods associated with the Check Out Form.
*/
class CkeckOut : public QWidget
{
    Q_OBJECT

public:
    //! CkeckOut constructor.
        /*!
          This constructor sets up the UI.
        */
    explicit CkeckOut(QWidget *parent = nullptr);

    //! CkeckOut destructor.
        /*!
         This destructs the check out UI.
        */
    ~CkeckOut();

private slots:

    //! Places an order for a customer and he or she's chosen service(s).
       /*!
         \return Nothing. Function is void.
       */
    void on_orderButton_clicked();

    //! Allows the user to return to the previous window.
       /*!
         \return Nothing. Function is void.
       */
    void on_backButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

    //! Error checks the inputted data.
       /*!
         \return Nothing. Function is void.
       */
    void errorCheck();

private:
    //! A private variable.
      /*!
        Pointer that points to the user interface of the checkout window
      */
    Ui::CkeckOut *ui;

    //! A private variable.
       /*!
         Allows the errorCheck() function to properly.
         \sa errorCheck()
       */
    bool badInput = false;
};

#endif // CKECKOUT_H
