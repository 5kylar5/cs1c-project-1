/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "databasemanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QMessageBox>

DatabaseManager::DatabaseManager()
{
    QDir directory;


    FilePath = directory.currentPath() + "/../iCyberSecurity/database.db";


    database = QSqlDatabase::addDatabase("QSQLITE");


    database.setDatabaseName(FilePath);

    if(!database.open()) {
        FilePath = directory.currentPath() + "/../../../../iCyberSecurity/database.db";
        database.setDatabaseName(FilePath);
        if(!database.open())
        {
            qDebug() << "Error Connecting Database";
            return;
        }
    }

    qDebug() << "Database Connection was Successful";
      QSqlQuery query;
      if(!query.exec("CREATE TABLE IF NOT EXISTS Customer (id INTEGER PRIMARY KEY UNIQUE, Name VARACHAR UNIQUE, Address VARACHAR, "
                 " City VARACHAR,  Zipcode VARACHAR,  State VARACHAR,  Importance VARACHAR, Intreset VARCHAR, Recieved INT(0));"))
      {
           qWarning() << "ERROR: " << query.lastError().text();
      }

}

DatabaseManager::~DatabaseManager() {
    if(this->database.open()) {
        qDebug() << "Database Connection has been Closed";

        database.close();
    }
}

 void DatabaseManager::GetList(Customer list[], int &size)
 {

     QSqlQuery query;

     if(!query.exec("SELECT * from Customer"))
     {
         qWarning() << "ERROR: " << query.lastError().text();

     }

     size = 0;

     while(query.next() && size < 100)
     {
          Customer newCustomer;
          //qDebug() << query.value(0).toInt();
          newCustomer.SetId(query.value(0).toInt());
          newCustomer.SetName(query.value(1).toString());
          newCustomer.SetAddress(query.value(2).toString());
          newCustomer.SetCity(query.value(3).toString());
          newCustomer.SetZip(query.value(4).toString());
          newCustomer.SetState(query.value(5).toString());
          newCustomer.SetImportance(query.value(6).toString());
          newCustomer.SetItrest(query.value(7).toString());
          newCustomer.SetPamphlet(query.value(8).toInt());
          list[size] = newCustomer;
          size++;
     }
 }

bool DatabaseManager::AddCustomer(Customer customer)
{
    QSqlQuery query;
    QString valueString= "('";

    //valueString += QString::number(customer.GetId()) + ",'";
    valueString += customer.GetName() + "','";
    valueString += customer.GetAddress() + "','";
    valueString += customer.GetCity() + "','";
    valueString += customer.GetZip() + "','";
    valueString += customer.GetState() + "','";
    valueString += customer.GetImportance() + "','";
    valueString += customer.GetIntrest() + "',";
    valueString += QString::number(customer.GetPamphlet()) + ");";

    if(!query.exec("INSERT INTO Customer (Name,Address,City,Zipcode,State,Importance,Intreset,Recieved) "
                   "VALUES " + valueString))
    {
        QMessageBox msgBox;
            msgBox.setText("Unable to add customer. Name must be unique.");
            msgBox.exec();
        return false;
    }

    return true;
}

Customer DatabaseManager::GetCustomerById(int Id)
{
    Customer newCustomer;
    QSqlQuery query;

    QString id = QString::number(Id);

    if(!query.exec("SELECT * from Customer WHERE id = " + id))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }
    if(query.first())
    {

        newCustomer.SetId(query.value(0).toInt());
        newCustomer.SetName(query.value(1).toString());
        newCustomer.SetAddress(query.value(2).toString());
        newCustomer.SetCity(query.value(3).toString());
        newCustomer.SetZip(query.value(4).toString());
        newCustomer.SetState(query.value(5).toString());
        newCustomer.SetImportance(query.value(6).toString());
        newCustomer.SetItrest(query.value(7).toString());
        newCustomer.SetPamphlet(query.value(8).toInt());
    }

    return newCustomer;
}


Customer DatabaseManager::GetCustomerByName(QString name)
{
    Customer newCustomer;
    QSqlQuery query;

    qDebug() << "Get  by name name: " << name[0];

    if(!query.exec("SELECT * from Customer WHERE Name = '" + name + "'"))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }
    if(query.first())
    {

        newCustomer.SetId(query.value(0).toInt());
        newCustomer.SetName(query.value(1).toString());
        newCustomer.SetAddress(query.value(2).toString());
        newCustomer.SetCity(query.value(3).toString());
        newCustomer.SetZip(query.value(4).toString());
        newCustomer.SetState(query.value(5).toString());
        newCustomer.SetImportance(query.value(6).toString());
        newCustomer.SetItrest(query.value(7).toString());
        newCustomer.SetPamphlet(query.value(8).toInt());
    }

    return newCustomer;
}

void DatabaseManager::DeleteCustomerById(int Id)
{
     QSqlQuery query;

     QString id = QString::number(Id);

     if(!query.exec("DELETE FROM Customer WHERE id = " + id))
     {
         qWarning() << "ERROR: " << query.lastError().text();

     }
}
void DatabaseManager::UpdateCustomer(Customer updatedCustomer)
{
    QSqlQuery query;

    QString setString = "";

    setString += "Name = '" + updatedCustomer.GetName() + "',";
    setString += "Address = '" + updatedCustomer.GetAddress() + "',";
    setString += "Zipcode = '" + updatedCustomer.GetZip() + "',";
    setString += "State = '" + updatedCustomer.GetState() + "',";
    setString += "City = '" + updatedCustomer.GetCity() + "',";
    setString += "Importance = '" + updatedCustomer.GetImportance() + "',";
    setString += "Intreset = '" + updatedCustomer.GetIntrest() + "',";
    setString += "Recieved = " + QString::number(updatedCustomer.GetPamphlet()) + "";

    qDebug() << setString;

    if(!query.exec("UPDATE Customer SET " + setString + " WHERE id = " + QString::number(updatedCustomer.GetId())))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }

}
