/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef ADMINEDITCUSTOMER_H
#define ADMINEDITCUSTOMER_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminEditCustomer.

    This namespace contains the class AdminEditCustomer.
*/
namespace Ui {
class AdminEditCustomer;
}

//!  AdminEditCustomer class.
/*!
  This class contains all the public and private data members/methods associated with the Edit Customer Form within the admin portal.
*/
class AdminEditCustomer : public QWidget
{
    Q_OBJECT

public:
    //! AdminEditCustomer constructor.
          /*!
            This constructor sets up the UI.
          */
    explicit AdminEditCustomer(QWidget *parent = nullptr);

    //! AdminEditCustomer destructor.
          /*!
           This destructs the edit customer UI.
          */
    ~AdminEditCustomer();

private slots:
    //! Allows the user to return to the admin portal.
           /*!
             \return Nothing. Function is void.
           */
    void on_backButton_clicked();

    //! Displays the Help for that window.
           /*!
             \return Nothing. Function is void.
           */
    void on_helpButton_clicked();

    //! Saves the new edited customer information.
           /*!
             \return Nothing. Function is void.
           */
    void on_saveButton_clicked();

    //! Error checks the inputted data.
           /*!
             \return Nothing. Function is void.
           */
    void errorCheck();

    //! Handles when the ID information is edited.
           /*!
             \return Nothing. Function is void.
           */
    void on_id_textEdited(const QString &arg1);

    //! Autofills the edit customer input fields.
           /*!
             \return Nothing. Function is void.
           */
    void on_saveButton_2_clicked();

private:

    //! A private variable.
          /*!
            Pointer that points to the user interface of the edit customer window
          */
    Ui::AdminEditCustomer *ui;

    //! A private variable.
          /*!
            Allows the errorCheck() function to properly.
            \sa errorCheck()
          */
    bool badInput = false;
};

#endif // ADMINEDITCUSTOMER_H
