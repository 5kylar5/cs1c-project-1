/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "loginscreen.h"
#include "ui_loginscreen.h"
#include "mainwindow.h"
#include "adminportal.h"
#include <QMessageBox>

LoginScreen::LoginScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginScreen)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}

LoginScreen::~LoginScreen()
{
    delete ui;
}

void LoginScreen::on_homeButton_clicked()
{
    MainWindow *main = new MainWindow();
    main -> show();
    this -> close();
}

void LoginScreen::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("This is the admin login screen. If you are a customer click \"Home\" to exit.");
        msgBox.exec();
}


void LoginScreen::on_password_returnPressed()
{
    QString password = this->ui->password->text();
    QString username = this->ui->username->text();

    QString usernames[4] = {"Richard","Skylar","Ramon","Shervin"};
    QString passwords[4] = {"Ram","Sarabia","Amini","Unknown"};

    if(username.length() == 0 || password.length() == 0)
    {
        QMessageBox::information(this,"Login Error","Please Enter a username and password");
        this->ui->password->setText("");
        return;
    }
    int i = 0;
    for (i = 0;i < 4; i++) {
        if(usernames[i] == username)
            break;
    }

    if(i >= 4 || passwords[i] != password)
    {
        QMessageBox::information(this,"Login Error","Incorrect Username or Password");
        this->ui->password->setText("");
        return;
    }

    AdminPortal *adminportal = new AdminPortal();
    adminportal -> show();
    this -> close();

}

void LoginScreen::on_loginButton_clicked()
{
    QString password = this->ui->password->text();
    QString username = this->ui->username->text();

    QString usernames[4] = {"Richard","Skylar","Ramon","Shervin"};
    QString passwords[4] = {"Ram","Sarabia","Amini","Beyk"};

    if(username.length() == 0 || password.length() == 0)
    {
        QMessageBox::information(this,"Login Error","Please Enter a username and password");
        this->ui->password->setText("");
        return;
    }
    int i = 0;
    for (i = 0;i < 4; i++) {
        if(usernames[i] == username)
            break;
    }

    if(i >= 4 || passwords[i] != password)
    {
        QMessageBox::information(this,"Login Error","Incorrect Username or Password");
        this->ui->password->setText("");
        return;
    }

    AdminPortal *adminportal = new AdminPortal();
    adminportal -> show();
    this -> close();
}
