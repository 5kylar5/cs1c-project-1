#include "purchasemanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QMessageBox>

PurchaseManager::PurchaseManager()
{
    QDir directory;


    FilePath = directory.currentPath() + "/../iCyberSecurity/database.db";


    database = QSqlDatabase::addDatabase("QSQLITE");


    database.setDatabaseName(FilePath);

    if(!database.open()) {
        FilePath = directory.currentPath() + "/../../../../iCyberSecurity/database.db";
        database.setDatabaseName(FilePath);
        if(!database.open())
        {
            qDebug() << "Error Connecting Database";
            return;
        }
    }

    qDebug() << "Database Connection was Successful";
      QSqlQuery query;
      if(!query.exec("CREATE TABLE IF NOT EXISTS Purchase (id INTEGER PRIMARY KEY UNIQUE, Name VARACHAR UNIQUE, Address VARACHAR, "
                 " City VARACHAR,  Zipcode VARACHAR,  State VARACHAR,  Email INT(0), Network INT(0), Cloud INT(0),"
                     "Maintence INT(0));"))
      {
           qWarning() << "ERROR: " << query.lastError().text();
      }


}

PurchaseManager::~PurchaseManager()
{
    if(this->database.open()) {
        qDebug() << "Database Connection has been Closed";

        database.close();
    }
}

bool PurchaseManager::AddPurchase(Purchase purchase)
{
    QSqlQuery query;
    QString valueString= "('";

    valueString += purchase.GetCustomer().GetName() + "','";
    valueString += purchase.GetCustomer().GetAddress() + "','";
    valueString += purchase.GetCustomer().GetCity() + "','";
    valueString += purchase.GetCustomer().GetZip() + "','";
    valueString += purchase.GetCustomer().GetState() + "',";
    valueString += QString::number(purchase.GetEmail()) + ",";
    valueString += QString::number(purchase.GetNetwork()) + ",";
    valueString += QString::number(purchase.GetCloud()) + ",";
    valueString += QString::number(purchase.GetMaintence()) + ");";

    if(!query.exec("INSERT INTO Purchase (Name,Address,City,Zipcode,State,Email,Network,Cloud,Maintence) "
                   "VALUES " + valueString))
    {
        return false;
    }

    return true;
}

Purchase PurchaseManager::GetPurchaseById(int Id)
{
    Purchase newPurchase;
    QSqlQuery query;

    QString id = QString::number(Id);

    if(!query.exec("SELECT * from Purchase WHERE id = " + id))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }
    if(query.first())
    {

        Customer cust;
        cust.SetName(query.value(1).toString());
        cust.SetAddress(query.value(2).toString());
        cust.SetZip(query.value(4).toString());
        cust.SetState(query.value(5).toString());
        cust.SetCity(query.value(3).toString());
        newPurchase.SetCustomer(cust);
        newPurchase.SetId(query.value(0).toInt());

        newPurchase.SetEmail(query.value(6).toInt());
        newPurchase.SetNetwork(query.value(7).toInt());
        newPurchase.SetCloud(query.value(8).toInt());
        newPurchase.SetMaintence(query.value(9).toInt());

        newPurchase.GetTotalPrice();
        newPurchase.BuildPlanString();
    }

    return newPurchase;
}

Purchase PurchaseManager::GetPurchaseByName(QString name)
{
    Purchase newPurchase;
    QSqlQuery query;

    if(!query.exec("SELECT * from Purchase WHERE Name = '" + name + "'"))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }
    if(query.first())
    {
        Customer cust;
        cust.SetName(query.value(1).toString());
        cust.SetAddress(query.value(2).toString());
        cust.SetZip(query.value(4).toString());
        cust.SetState(query.value(5).toString());
        cust.SetCity(query.value(3).toString());
        newPurchase.SetCustomer(cust);
        newPurchase.SetId(query.value(0).toInt());

        newPurchase.SetEmail(query.value(6).toInt());
        newPurchase.SetNetwork(query.value(7).toInt());
        newPurchase.SetCloud(query.value(8).toInt());
        newPurchase.SetMaintence(query.value(9).toInt());

        newPurchase.GetTotalPrice();
        newPurchase.BuildPlanString();
    }

    return newPurchase;
}

void PurchaseManager::DeletePurchaseById(int Id)
{
    QSqlQuery query;

    QString id = QString::number(Id);

    if(!query.exec("DELETE FROM Purchase WHERE id = " + id))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }
}

void PurchaseManager::GetList(Purchase list[], int &size)
{
    QSqlQuery query;

    if(!query.exec("SELECT * from Purchase"))
    {
        qWarning() << "ERROR: " << query.lastError().text();

    }

    size = 0;

    while(query.next() && size < 100)
    {
         Purchase newPurchase;
         Customer cust;
         cust.SetName(query.value(1).toString());
         cust.SetAddress(query.value(2).toString());
         cust.SetZip(query.value(4).toString());
         cust.SetState(query.value(5).toString());
         cust.SetCity(query.value(3).toString());
         newPurchase.SetCustomer(cust);
         newPurchase.SetId(query.value(0).toInt());

         newPurchase.SetEmail(query.value(6).toInt());
         newPurchase.SetNetwork(query.value(7).toInt());
         newPurchase.SetCloud(query.value(8).toInt());
         newPurchase.SetMaintence(query.value(9).toInt());

         newPurchase.GetTotalPrice();
         newPurchase.BuildPlanString();

         list[size] = newPurchase;
         size++;
    }
}
