/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef RECEIPT_H
#define RECEIPT_H

#include <QWidget>

/*! \namespace Ui
    \brief class Receipt.

    This namespace contains the class Receipt.
*/
namespace Ui {
class Receipt;
}

//!  Receipt class.
/*!
  This class contains all the public and private data members/methods associated with the Receipt.
*/
class Receipt : public QWidget
{
    Q_OBJECT

public:
    //! Receipt constructor.
       /*!
         This constructor sets up the UI.
       */
    explicit Receipt(QWidget *parent = nullptr);    

    //! Receipt non default constructor.
       /*!
         This constructor sets up the receipt according to the parameters.
         \param parent the parent
         \param name the name
       */
    Receipt(QWidget *parent = nullptr, QString name = "");

    //! Receipt destructor.
        /*!
         This destructs Receipt.
        */
    ~Receipt();

private slots:
    //! Allows the user to return to previous window.
       /*!
         \return Nothing. Function is void.
       */
    void on_homeButton_clicked();

    //! Displays the Help for that window.
       /*!
         \return Nothing. Function is void.
       */
    void on_helpButton_clicked();

private:

    //! A private variable.
      /*!
        Pointer that points to the user interface of the Receipt
      */
    Ui::Receipt *ui;
};

#endif // RECEIPT_H
