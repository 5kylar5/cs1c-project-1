/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#ifndef ADMINPORTAL_H
#define ADMINPORTAL_H

#include <QWidget>

/*! \namespace Ui
    \brief class AdminPortal.

    This namespace contains the class AdminPortal.
*/
namespace Ui {
class AdminPortal;
}

//!  AdminPortal class.
/*!
  This class contains all the public and private data members/methods associated with the Admin Portal.
*/
class AdminPortal : public QWidget
{
    Q_OBJECT

public:

    //! AdminPortal constructor.
           /*!
             This constructor sets up the UI.
           */
    explicit AdminPortal(QWidget *parent = nullptr);

    //! AdminPortal destructor.
            /*!
             This destructs the admin portal UI.
            */
    ~AdminPortal();

private slots:

    //! Logouts the admin and goes back to the main screen.
           /*!
             \return Nothing. Function is void.
           */
    void on_logoutButton_clicked();

    //! Displays the Help for that window.
           /*!
             \return Nothing. Function is void.
           */
    void on_helpButton_clicked();

    //! Opens the customer list UI.
           /*!
             \return Nothing. Function is void.
           */
    void on_showButton_clicked();

    //! Opens the add customer UI.
           /*!
             \return Nothing. Function is void.
           */
    void on_addButton_clicked();

    //! Opens remove customer UI.
           /*!
             \return Nothing. Function is void.
           */
    void on_removeButton_clicked();

    //! Opens the edit customer UI.
           /*!
             \return Nothing. Function is void.
           */
    void on_editButton_clicked();

    //! Opens the view purchases UI.
           /*!
             \return Nothing. Function is void.
           */
    void on_viewPurchasesButton_clicked();


private:

    //! A private variable.
         /*!
           Pointer that points to the user interface of the admin portal window
         */
    Ui::AdminPortal *ui;
};


#endif // ADMINPORTAL_H
