#include "testimonialcalss.h"

TestimonialCalss::TestimonialCalss()
{

}

TestimonialCalss::TestimonialCalss(class Customer customer, int id, QString text)
{
    Customer = customer;
    Id = id;
    Text = text;
}

TestimonialCalss::~TestimonialCalss()
{

}

void TestimonialCalss::SetCustomer(class Customer customer)
{
    Customer = customer;
}

void TestimonialCalss::SetId(int id)
{
    Id= id;
}

void TestimonialCalss::SetText(QString text)
{
    Text = text;
}

Customer TestimonialCalss::GetCustomer()
{
    return Customer;
}

int TestimonialCalss::GetId()
{
    return Id;
}

QString TestimonialCalss::GetText()
{
    return Text;
}
