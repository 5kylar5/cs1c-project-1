/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#include "customerlist.h"
#include "ui_customerlist.h"
#include <QDebug>
#include "databasemanager.h"


CustomerList::CustomerList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomerList)
{
    ui->setupUi(this);
    ui->customerTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QStringList columnTitles;

    ui->customerTable->setColumnCount(6);
    columnTitles << "ID" << "Name" << "Address" << "Interest" << "Importance" << "Req. Pamphlet"; //<< "Request Status";
    ui->customerTable->setHorizontalHeaderLabels(columnTitles);

    ui->customerTable->verticalHeader()->setVisible(false); // Removes first vertical column

    Customer list[100];
    int size = -1;
    DatabaseManager db;
    db.GetList(list,size);

    int index = 0;

    if(size == -1)
    {
        return;
    }
    ui->customerTable->setRowCount(size);
    while(index <= size)
    {
         QString address = list[index].GetAddress() + " " + list[index].GetCity() + " " + list[index].GetState() + " " + list[index].GetZip();

         ui->customerTable->setItem(index,0,new QTableWidgetItem(QString::number(list[index].GetId())));
         ui->customerTable->setItem(index,1,new QTableWidgetItem(list[index].GetName()));
         ui->customerTable->setItem(index,2,new QTableWidgetItem(address));
         ui->customerTable->setItem(index,3,new QTableWidgetItem(list[index].GetIntrest()));
         ui->customerTable->setItem(index,4,new QTableWidgetItem(list[index].GetImportance()));

         QString status = "NO";

        if(list[index].GetPamphlet())
            status = "YES";

         ui->customerTable->setItem(index,5,new QTableWidgetItem(status));
         index++;
    }







    //Should call SortByName()
}

CustomerList::~CustomerList()
{
    delete ui;
}

void CustomerList::SortByName()
{
    //Sorts the Customer List by Name Req
}

void CustomerList::SortByKey()
{
    //Sorts the Customer List by Name Req
}
