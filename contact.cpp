/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "contact.h"
#include "pamphlet.h"
#include "ui_contact.h"
#include <QMessageBox>

Contact::Contact(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Contact)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());
}

Contact::~Contact()
{
    delete ui;
}

void Contact::on_backButton_clicked()
{
    Pamphlet *pamphlet = new Pamphlet();
    pamphlet -> show();
    this -> close();
}

void Contact::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("To contact us, enter your name, email address, message, and click \"Send\". ");
        msgBox.exec();
}


void Contact::on_sendButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Thank you for reaching out to the ICyberSecurity Team. If you have any further questions let us know!");
    msgBox.exec();

    errorCheck();

    if(badInput == false)
    {
        Pamphlet *pamphlet = new Pamphlet();
        pamphlet -> show();
        this -> close();
    }

}


void Contact::errorCheck()
{
    badInput = false;

    if(ui->name->text() == "")
    {
        badInput = true;
        ui->nameLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->email->text() == "")
    {
         badInput = true;
        ui->emailLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->emailLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->message->toPlainText() == "")
    {
         badInput = true;
        ui->messageLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->messageLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

}
