/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/
#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QString>

//!  Customer class.
/*!
  This class contains all the public and private data members/methods associated with a customer.
*/
class Customer
{
public:
    //! Customer default constructor.
          /*!
            This constructor sets ID to 0
          */
    Customer();

    //! Customer non default constructor.
          /*!
            This constructor sets private data memebers to their associated parameters.
            \param name the name of customer
            \param address the address of customer
            \param city the city of the customer
            \param state the state of the customer
            \param zip the zip code of customer
            \param importance the level of importance
            \param intrest interest level of customer
            \param pamphlet customer pamphlet status
          */
    Customer(QString name, QString address, QString city,
             QString state, QString zip, QString importance,
             QString intrest, int pamphlet);


    //! Customer copy constructor.
          /*!
            This is the copy constructor
            \param customer passed by reference
          */
    Customer(const Customer& customer);

    //! Customer destructor.
        /*!
         This destructs the customer.
        */
    ~Customer();

    //! Sets the customers ID.
        /*!
          \param id the id
          \return Nothing. Function is void.
        */
    void SetId(int id);

    //! Sets the name.
        /*!
          \param name the name
          \return Nothing. Function is void.
        */
    void SetName(QString name);

    //! Sets the address.
        /*!
          \param address the address
          \return Nothing. Function is void.
        */
    void SetAddress(QString address);

    //! Sets the city.
        /*!
          \param city the city
          \return Nothing. Function is void.
        */
    void SetCity(QString city);

    //! Sets the state.
        /*!
          \param state the state
          \return Nothing. Function is void.
        */
    void SetState(QString state);

    //! Sets the zipcode.
        /*!
          \param zip the zipcode
          \return Nothing. Function is void.
        */
    void SetZip(QString zip);

    //! Sets the importance level.
        /*!
          \param importance the importance level of the customer
          \return Nothing. Function is void.
        */
    void SetImportance(QString importance);

    //! Sets interest level.
        /*!
          \param intrest the interest level of the customer
          \return Nothing. Function is void.
        */
    void SetItrest(QString intrest);

    //! Sets pamphlet status.
        /*!
          \param pamphlet the pamphlet status of the customer
          \return Nothing. Function is void.
        */
    void SetPamphlet(int pamphlet);

    //! Gets ID.
        /*!
          \return int - the ID.
        */
    int GetId();

    //! Gets name.
        /*!
          \return the name.
        */
    QString GetName();

    //! Gets address.
        /*!
          \return the address.
        */
    QString GetAddress();

    //! Gets city.
        /*!
          \return the city.
        */
    QString GetCity();

    //! Gets state.
        /*!
          \return the state.
        */
    QString GetState();

    //! Gets zip.
        /*!
          \return the zip.
        */
    QString GetZip();

    //! Gets importance level.
        /*!
          \return the importance.
        */
    QString GetImportance();

    //! Gets Interest.
        /*!
          \return the interest level of customer.
        */
    QString GetIntrest();

    //! Gets Pamphlet Status.
        /*!
          \return the pamphlet status.
        */
    int GetPamphlet();

private:
    //! A private variable.
      /*!
        ID of customer
      */
    int Id;

    //! A private variable.
      /*!
        Name of customer
      */
    QString Name;

    //! A private variable.
      /*!
        Address of customer
      */
    QString Address;

    //! A private variable.
      /*!
        City of customer
      */
    QString City;

    //! A private variable.
      /*!
        State of customer
      */
    QString State;

    //! A private variable.
      /*!
        Zip of customer
      */
    QString ZipCode;

    //! A private variable.
      /*!
        Importance level of customer
      */
    QString ImportanceLevel;

    //! A private variable.
      /*!
        Interest level of customer
      */
    QString IntrestLevel;

    //! A private variable.
      /*!
        Pamphlet status of customer
      */
    int Pamphlet;


};

#endif // CUSTOMER_H
