/*******************************************************************************
 * AUTHOR         : Shervin Beyk & Skylar Sarabia & Ramon Amini & Richard Ram
 * STUDENT ID     : 1088216      & 1091626        & 1086060     & 1051511
 * PROJECT 1      : iCyberSecurity
 * CLASS          : CS1C
 * SECTION        : MW 5:00PM
 * DUE DATE       : 3/25/19
 *******************************************************************************/

#include "adminaddcustomer.h"
#include "ui_adminaddcustomer.h"
#include "adminportal.h"
#include <QMessageBox>
#include <QDebug>
#include "databasemanager.h"

AdminAddCustomer::AdminAddCustomer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminAddCustomer)
{
    ui->setupUi(this);
    this->setWindowTitle("iCyberSecurity");
    this->setFixedSize(this->size());

}

AdminAddCustomer::~AdminAddCustomer()
{
    delete ui;
}

void AdminAddCustomer::on_addButton_clicked()
{
    //Assigns text input fields to local vaiables
    QString name    = ui->name->text();
    QString street  = ui->address->text();
    QString city    = ui->city->text();
    QString state   = ui->state->text();
    QString zipcode = ui->zipcode->text();

    //Concatentates address fields to make address a single string
    QString address = street + " " + city + " " + state + " " + zipcode;

    //Grabs the text form the three pickers
    QString intrestLvl = ui->interestLevelPicker->itemText(ui->interestLevelPicker->currentIndex());
    QString importance = ui->importancePicker->itemText(ui->importancePicker->currentIndex());
    QString received   = ui->receivedPamphletPicker->itemText(ui->receivedPamphletPicker->currentIndex());

    errorCheck();

    int pamphlet = 0;
    if(received == "yes")
        pamphlet = 1;

    //ADD CUSTOMER TO DATABASE

    //ADD A CUSTOMER FIRST


   if(badInput == false)
   {
       DatabaseManager database;

       Customer customer;
       customer.SetAddress(street);
       customer.SetZip(zipcode);
       customer.SetState(state);
       customer.SetCity(city);
       customer.SetName(name);
       customer.SetItrest(intrestLvl);
       customer.SetImportance(importance);
       customer.SetPamphlet(pamphlet);

       bool valid = database.AddCustomer(customer);

       if(valid)
       {
           QMessageBox::StandardButton reply = QMessageBox::question(this, "Add Customer", "Successfully added customer. Would you like to add another customer?",
                                                                     QMessageBox::Yes | QMessageBox::No);
           if(reply == QMessageBox::No)
           {
               AdminPortal *adminPortal = new AdminPortal();
               adminPortal -> show();
               this -> close();
           }
           else
           {
               AdminAddCustomer *adminAd = new AdminAddCustomer();
               adminAd -> show();
               this -> close();
           }
       }

   }



    //ADD A CUSTOMER FIRST
}

void AdminAddCustomer::errorCheck()
{
    badInput = false;

    if(ui->name->text() == "")
    {
        badInput = true;
        ui->nameLbl->setStyleSheet("color: rgb(252, 0, 6);");

    }
    else
    {
        ui->nameLbl->setStyleSheet("color: rgb(255, 255, 255);");

    }

    if(ui->address->text() == "")
    {
         badInput = true;
        ui->addressLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->addressLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->city->text() == "")
    {
         badInput = true;
        ui->cityLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->cityLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->zipcode->text() == "")
    {
         badInput = true;
        ui->zipcodeLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->zipcodeLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->state->text() == "")
    {
         badInput = true;
        ui->stateLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->stateLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->receivedPamphletPicker->currentIndex()==-1)
    {
         badInput = true;
        ui->receivedPamphletLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->receivedPamphletLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->importancePicker->currentIndex()==-1)
    {
         badInput = true;
        ui->importanceLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->importanceLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

    if(ui->interestLevelPicker->currentIndex()==-1)
    {
         badInput = true;
        ui->interestLevelLbl->setStyleSheet("color: rgb(252, 0, 6);");
    }
    else{
        ui->interestLevelLbl->setStyleSheet("color: rgb(255, 255, 255);");
    }

}

void AdminAddCustomer::on_helpButton_clicked()
{
    QMessageBox msgBox;
        msgBox.setText("Add customer info to add a new customer to the list of customers. Or click back to exit.");
        msgBox.exec();
}

void AdminAddCustomer::on_backButton_clicked()
{
    AdminPortal *adminPortal = new AdminPortal();
    adminPortal -> show();
    this -> close();
}
